// next-prev buttons for tabs
jQuery('.next-link').click(function(){
  jQuery('.nav-tabs > .active').next('li').find('a').trigger('click');
});

  jQuery('.back-link').click(function(){
  jQuery('.nav-tabs > .active').prev('li').find('a').trigger('click');
});
// custom checkboxes and radios
jQuery('.same-mailing_checkbox').on('click', function(){
  if(jQuery(this).find('input').is(':checked')){
    jQuery(this).find('label').toggleClass('checked');
    jQuery('.remittance-hidden').removeClass('hidden');
  } else {
  	jQuery('.remittance-hidden').addClass('hidden');
  }
})

jQuery('.business-info_radio label').on('click', function(){
    jQuery(this).parent().find('label').removeClass('checked');
    jQuery('.business-info_section').addClass('hidden');
    if(jQuery(this).find('input').is(':checked')){
    	jQuery(this).addClass('checked');
    	var currentOption = jQuery(this).find('input')[0].value;
    	switch(currentOption){
    		case "Partnership":
    			jQuery('.partnership-section').removeClass('hidden');
    			break;
  			case "Corporation":
    			jQuery('.corporation-section').removeClass('hidden');
    			break;
  			case "SoleProprietor":
    			jQuery('.sole-section').removeClass('hidden');
    			break;
    	}
    }
});
jQuery('.business-info_union_radio label').on('click', function(){
    jQuery(this).parent().find('label').removeClass('checked');
    if(jQuery(this).find('input').is(':checked')){
    	jQuery(this).addClass('checked');
    }
    var currentOption = jQuery(this).find('input')[0].value;
    if(currentOption == 'Yes'){
    	jQuery('#business-info_corporation_union_text').prop('disabled', false);
    } else {
    	jQuery('#business-info_corporation_union_text').prop('disabled', true);
    }
});
jQuery('.previouswork_radio label').on('click', function(){
    jQuery(this).parent().find('label').removeClass('checked');
    if(jQuery(this).find('input').is(':checked')){
    	jQuery(this).addClass('checked');
    }
    var currentOption = jQuery(this).find('input')[0].value;
    if(currentOption == 'Yes'){
    	jQuery('.previouswork-selectable-section').find('input:not([type="checkbox"])').prop('disabled', false);
    } else {
    	jQuery('.previouswork-selectable-section').find('input:not([type="checkbox"])').prop('disabled', true);
    }
});
jQuery('.bonding_checkbox').on('click', function(){
  if(jQuery(this).find('input').is(':checked')){
    jQuery(this).find('label').toggleClass('checked');
    jQuery('.financial-bonding').find('input:not([type="checkbox"])').prop('disabled', true);
  } else {
  	jQuery('.financial-bonding').find('input:not([type="checkbox"])').prop('disabled', false);
  }
})

//appending number of employees at businnes information section
jQuery('#business-info_partnership_number-employees').on('change', function(){
	var numberEmployees = +jQuery(this)[0].value;
	var container = jQuery('.business-partners_blocks');
	container.html('');
	for(var i = 1, len = numberEmployees + 1; i < len; i++){
		var block ='<hr>'+
						'<div class="form-group form-inline row business-info_partnership_namepartner' + i + '">'+
						    '<label class="col-md-3" for="business-info_partnership_namepartner'+experienceProjectsNum+'">Name of Partner #'+ i + ':</label>'+
						    '<div class="col-md-9">'+
						    	'<input type="text" class="form-control w100" id="business-info_partnership_namepartner'+ i +'" placeholder="enter your text here...">'+
						    '</div>'+
						'</div>'+
						'<div class="form-group form-inline row business-info_partnership_mailpartner'+ i + '">'+
						    '<label class="col-md-3" for="business-info_partnership_mailpartner'+ i + '">Mailing Address:</label>'+
						    '<div class="col-md-9">'+
						    	'<input type="text" class="form-control w100" id="business-info_partnership_mailpartner'+ i + '" placeholder="Street Address">'+
						    '</div>'+
						'</div>'+
						'<div class="form-group form-inline row partner-mailing-address-'+ i + '">'+
					    '<div class="col-md-5">'+
					    	'<input type="text" class="form-control w100" id="partner_mailingCity'+ i + '" placeholder="City">'+
					    '</div>'+
					    '<div class="col-md-5">'+
					    	'<select name="partner_mailingProvince'+ i + '" id="partner_mailingProvince'+ i + '" class="form-control w100">'+
					    		'<option value="" selected disabled hidden>Province / Territory:</option>'+
					    		'<option value="Alberta">Alberta</option>'+
					    		'<option value="British Columbia">British Columbia</option>'+
					    		'<option value="Manitoba">Manitoba</option>'+
					    		'<option value="New Brunswick">New Brunswick</option>'+
									'<option value="Newfoundland and Labrador">Newfoundland and Labrador</option>'+
									'<option value="Northwest Territories">Northwest Territories</option>'+
									'<option value="Nova Scotia">Nova Scotia</option>'+
									'<option value="Nunavut">Nunavut</option>'+
									'<option value="Ontario">Ontario</option>'+
									'<option value="Prince Edward Island">Prince Edward Island</option>'+
									'<option value="Quebec">Quebec</option>'+
									'<option value="Saskatchewan">Saskatchewan</option>'+
									'<option value=""></option>'+
									'	</select>'+
									'</div>'+
									'<div class="col-md-2">'+
									'	<input type="text" class="form-control w100" id="parnter_mailingPostalCode'+ i + '" placeholder="Postal code">'+
									'</div>'+
						'</div>'
		container.append(block);
	}
})

//appending number of trades at financial section
jQuery('#financial-trades_number').on('change', function(){
	var numberTrades = +jQuery(this)[0].value;
	var container = jQuery('.financial-trades_trades-list_wrapper');
	container.html('');
	for(var i = 1, len = numberTrades + 1; i < len; i++){
		var block = 	'<div class="financial-trades_single-trade'+ i +' row">'+
'										<div class="col-md-8">'+
'											<div class="form-group form-inline row">                            '+
'											    <label class="col-md-3" for="financial-trades_single-trade_name'+ i +'">Trade:</label>'+
'											    <div class="col-md-8">'+
'											    	<input type="text" class="form-control w100" id="financial-trades_single-trade_name'+ i +'" placeholder="enter your text here...">'+
'											    </div>'+
'											</div>'+
'										</div>'+
'										<div class="col-md-4">'+
'								    	<select name="financial-trades_single-trade_range'+ i +'" id="financial-trades_single-trade_range'+ i +'" class="form-control">'+
'								    		<option value="" selected disabled hidden>Range:</option>'+
'												<option value="'+experienceProjectsNum+'">1</option>'+
'												<option value="2">2</option>'+
'												<option value="3">3</option>'+
'												<option value="4">4</option>'+
'												<option value="5">5</option>'+
'												<option value="6">6</option>'+
'												<option value="7">7</option>'+
'												<option value="8">8</option>'+
'												<option value="9">9</option>'+
'												<option value="10">10</option>     '+
'								    	</select>'+
'										</div>'+
'									</div>'
		container.append(block);
	}
})

// append new block at experience section
var experienceProjectsNum = 2;
jQuery('.experience_btn-add-new-project').on('click', function(e){
	e.preventDefault();
	var container = jQuery('.projects-list-section');
	var block = '<div class="forms-single-block experience_project project'+experienceProjectsNum+' rounded8">'+
'								<div class="form-group form-inline row">                            '+
'								    <label class="col-md-3 blue" for="experience_project_description'+experienceProjectsNum+'">Project Description:</label>'+
'								    <div class="col-md-9">'+
'								    	<input type="text" class="form-control w100" id="experience_project_description'+experienceProjectsNum+'" placeholder="enter your text here...">'+
'								    </div>'+
'								</div>'+
'								<hr>'+
'								<div class="form-group form-inline row"> '+
'								    <label class="col-md-3 blue" for="experience_project_year'+experienceProjectsNum+'">Year of Completion:</label>'+
'								    <div class="col-md-9">'+
'								    	<div class="row">'+
'								    		<div class="col-md-3">'+
'										    	<select name="experience_project_year'+experienceProjectsNum+'" id="experience_project_year'+experienceProjectsNum+'" class="form-control">'+
'										    		<option value="" selected disabled hidden>Year:</option>'+
'														<option value="2009">2009</option>       '+
'														<option value="2010">2010</option>       '+
'														<option value="201'+experienceProjectsNum+'">2011</option>       '+
'														<option value="2012">2012</option>       '+
'														<option value="2013">2013</option>       '+
'														<option value="2014">2014</option>       '+
'														<option value="2015">2015</option>       '+
'														<option value="2016">2016</option>       '+
'														<option value="2017">2017</option>       '+
'														<option value="2018">2018</option>       '+
'										    	</select>'+
'								    		</div>'+
'								    		<div class="col-md-9">'+
'								    			<div class="row experience_project_value_wrapper">'+
'								    				<label class="col-md-4" for="experience_project_value'+experienceProjectsNum+'">'+
'								    					Project Value:'+
'								    				</label>'+
'								    				<div class="col-md-8">'+
'								    					<input type="text" class="form-control w100" id="experience_project_value'+experienceProjectsNum+'" placeholder="enter your text here...">'+
'								    				</div>'+
'								    			</div>'+
'								    		</div>'+
'								    	</div>'+
'								    </div>'+
'								</div>'+
'								<hr>'+
'								<div class="form-group form-inline row">                            '+
'								    <label class="col-md-3" for="experience_project_ownersname'+experienceProjectsNum+'">Owner’s Name:</label>'+
'								    <div class="col-md-9">'+
'								    	<input type="text" class="form-control w100" id="experience_project_ownersname'+experienceProjectsNum+'" placeholder="enter your text here...">'+
'								    </div>'+
'								</div>'+
'								<hr>'+
'								<div class="form-group form-inline row">                            '+
'								    <label class="col-md-3" for="experience_project_address'+experienceProjectsNum+'">Project Address:</label>'+
'								    <div class="col-md-9">'+
'								    	<input type="text" class="form-control w100" id="experience_project_address'+experienceProjectsNum+'" placeholder="enter your text here...">'+
'								    </div>'+
'								</div>'+
'								<div class="form-group form-inline row">'+
'							    <div class="col-md-5">'+
'							    	<input type="text" class="form-control w100" id="experience_project_mailingCity'+experienceProjectsNum+'" placeholder="City">'+
'							    </div>'+
'							    <div class="col-md-5">'+
'							    	<select name="experience_project_mailingProvince'+experienceProjectsNum+'" id="experience_project_mailingProvince'+experienceProjectsNum+'" class="form-control w100">'+
'							    		<option value="" selected disabled hidden>Province / Territory:</option>'+
'							    		<option value="Alberta">Alberta</option>'+
'							    		<option value="British Columbia">British Columbia</option>'+
'							    		<option value="Manitoba">Manitoba</option>'+
'							    		<option value="New Brunswick">New Brunswick</option>'+
'							    		<option value="Newfoundland and Labrador">Newfoundland and Labrador</option>'+
'							    		<option value="Northwest Territories">Northwest Territories</option>'+
'							    		<option value="Nova Scotia">Nova Scotia</option>'+
'							    		<option value="Nunavut">Nunavut</option>'+
'							    		<option value="Ontario">Ontario</option>'+
'							    		<option value="Prince Edward Island">Prince Edward Island</option>'+
'							    		<option value="Quebec">Quebec</option>'+
'							    		<option value="Saskatchewan">Saskatchewan</option>'+
'							    		<option value=""></option>'+
'							    	</select>'+
'							    </div>'+
'							    <div class="col-md-2">'+
'							    	<input type="text" class="form-control w100" id="experience_project_mailingPostalCode'+experienceProjectsNum+'" placeholder="Postal code">'+
'							    </div>'+
'								</div>'+
'								<hr>'+
'								<div class="form-group form-inline row">                            '+
'								    <label class="col-md-3" for="experience_project_ownerscontact1_'+experienceProjectsNum+'">Owner’s Contact:</label>'+
'								    <div class="col-md-9">'+
'								    	<input type="text" class="form-control w100" id="experience_project_ownerscontact1_'+experienceProjectsNum+'" placeholder="enter your text here...">'+
'								    </div>'+
'								</div>'+
'								<div class="row cols2-phone-wrapper">'+
'									<div class="col-md-6">'+
'										<div class="form-group row experience_project-phone1_'+experienceProjectsNum+'">'+
'									    <label class="col-md-12" for="experience_project-phone1_'+experienceProjectsNum+'">Phone #:</label>'+
'									    <div class="col-md-12 cols3-phone">'+
'									    	<input type="text" class="form-control" id="experience_project-phone1_1-'+experienceProjectsNum+'" placeholder=""> -'+
'									    	<input type="text" class="form-control" id="experience_project-phone1_2-'+experienceProjectsNum+'" placeholder=""> -'+
'									    	<input type="text" class="form-control" id="experience_project-phone1_3-'+experienceProjectsNum+'" placeholder="">'+
'									    </div>'+
'										</div>'+
'									</div>'+
'									<div class="col-md-6">'+
'										<div class="form-group row experience_project-email1_'+experienceProjectsNum+'">'+
'									    <label class="col-md-12" for="experience_project-email'+experienceProjectsNum+'">Email Address:</label>'+
'									    <div class="col-md-12">'+
'									    	<input type="email" class="form-control w100" id="experience_project-email1_'+experienceProjectsNum+'" placeholder="enter your text here...">'+
'									    </div>'+
'										</div>'+
'									</div>'+
'								</div>'+
'								<hr>'+
'								<div class="form-group form-inline row">                            '+
'								    <label class="col-md-3" for="experience_project_ownerscontact2_'+experienceProjectsNum+'">Owner’s Contact:</label>'+
'								    <div class="col-md-9">'+
'								    	<input type="text" class="form-control w100" id="experience_project_ownerscontact2_'+experienceProjectsNum+'" placeholder="enter your text here...">'+
'								    </div>'+
'								</div>'+
'								<div class="form-group form-inline row">                            '+
'								    <label class="col-md-3" for="experience_project_consultantcontact'+experienceProjectsNum+'">Consultant’s Contact:</label>'+
'								    <div class="col-md-9">'+
'								    	<input type="text" class="form-control w100" id="experience_project_consultantcontact'+experienceProjectsNum+'" placeholder="enter your text here...">'+
'								    </div>'+
'								</div>'+
'								<div class="row cols2-phone-wrapper">'+
'									<div class="col-md-6">'+
'										<div class="form-group row experience_project-phone2_'+experienceProjectsNum+'">'+
'									    <label class="col-md-12" for="experience_project-phone2_'+experienceProjectsNum+'">Phone #:</label>'+
'									    <div class="col-md-12 cols3-phone">'+
'									    	<input type="text" class="form-control" id="experience_project-phone2_1-'+experienceProjectsNum+'" placeholder=""> -'+
'									    	<input type="text" class="form-control" id="experience_project-phone2_2-'+experienceProjectsNum+'" placeholder=""> -'+
'									    	<input type="text" class="form-control" id="experience_project-phone2_3-'+experienceProjectsNum+'" placeholder="">'+
'									    </div>'+
'										</div>'+
'									</div>'+
'									<div class="col-md-6">'+
'										<div class="form-group row experience_project-email2_'+experienceProjectsNum+'">'+
'									    <label class="col-md-12" for="experience_project-email2_'+experienceProjectsNum+'">Email Address:</label>'+
'									    <div class="col-md-12">'+
'									    	<input type="email" class="form-control w100" id="experience_project-email2_'+experienceProjectsNum+'" placeholder="enter your text here...">'+
'									    </div>'+
'										</div>'+
'									</div>'+
'								</div>'+
'							</div>';
	container.append(block);
	experienceProjectsNum+=1;
})