

$('.contactbtn').click(function(){
		var name = $('#name').val();
		var email = $('#email').val();
		var phone = $('#phone').val();
		var comment = $('#comment').val();

		if(name.length==0){
			alert('Enter your name !');
		}else if(email.length==0){
			alert('Enter your email !');
		}else if(!validateEmail(email)){
			alert('Enter right email !');
		}else if(phone.length==0){
			alert('Enter your phone !');
		}else{
			$.post('php/mail.php',{name:name,email:email,phone:phone,comment:comment},function(data){
				$('#name').val("");
				$('#email').val("");
				$('#phone').val("");
				$('#comment').val("");
				alert('Thank you! Your message sent.');
			});
		}
		
	});



function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}